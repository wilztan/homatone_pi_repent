from System import config
from Manager.Door import Door
from Manager.Downloader import Downloader
from Manager.Light import Light
from Manager.Media import Media
from Manager.Assistant import Assistant
from Manager.TemperatureInfraRed import Temperature
from Service.NetworkService import NetworkService
import threading


def event_manager():

    # initialize Door
    main_door = Door("Main Door", 12, 2.5, 5)
    window_door = Door("Window Door", 16, 5, 17.5)
    bed_room_door = Door("Bedroom Door", 13, 10, 2.5)

    # initialize Lights
    main_room_light = Light("Main Room", 7)
    toilet_room_light = Light("Toilet Room", 15)
    bed_room_light = Light("Bed Room", 11)

    main_room_thread = threading.Thread(name="Main Room", target=main_room_light.start_light)
    bed_room_thread = threading.Thread(name="Bed Room", target=bed_room_light.start_light)
    toilet_room_thread = threading.Thread(name="Toilet Room", target=toilet_room_light.start_light)

    main_room_thread.start()
    bed_room_thread.start()
    toilet_room_thread.start()

    # initialize Media
    media_player = Media()
    media_player_thread = threading.Thread(name="Media Player", target=media_player.start_media_player)
    media_player_thread.start()

    # initialize Connector
    downloader = Downloader()

    # initialize Assistant
    assistant = Assistant()

    # initialize temperature
    air_conditioner_main = Temperature("Main AC", 32)
    air_conditioner_thread = threading.Thread(
        name=air_conditioner_main.name,
        target=air_conditioner_main.start_machine
    )
    air_conditioner_thread.start()

    def event_separator(message):
        print("Received " + str(message))
        if message.get('hardware_id') == config.hardware_id:
            print("Calling Hardware")
            if message.get('request_type') == "DOOR_OPEN":
                if message.get('content') == "DOOR_MAIN":
                    main_door.open_close()

                if message.get('content') == "WINDOWS_MAIN_OPEN":
                    window_door.open()

                if message.get('content') == "WINDOWS_MAIN_CLOSE":
                    window_door.close()

                if message.get('content') == "DOOR_BED_OPEN":
                    bed_room_door.open()

                if message.get('content') == "DOOR_BED_CLOSE":
                    bed_room_door.close()

            if message.get('request_type') == "LIGHTS_MANAGE":
                if message.get('content') == "LIGHTS_MAIN_ON":
                    main_room_light.lights_on()
                if message.get('content') == "LIGHTS_MAIN_OFF":
                    main_room_light.lights_off()
                if message.get('content') == "LIGHTS_BED_ON":
                    bed_room_light.lights_on()
                if message.get('content') == "LIGHTS_BED_OFF":
                    bed_room_light.lights_off()
                if message.get('content') == "LIGHTS_TOILET_ON":
                    toilet_room_light.lights_on()
                if message.get('content') == "LIGHTS_TOILET_OFF":
                    toilet_room_light.lights_off()

            if message.get('request_type') == "IRMESSAGE":
                if message.get('content') == "STATUSAC:false":
                    air_conditioner_main.turn_on()
                if message.get('content') == "STATUSAC:true":
                    air_conditioner_main.turn_off()

            if message.get('request_type') == "NEWS_READ":
                print("VOICE OUT NEWS")
                assistant.read_news()

            if message.get('request_type') == "GOOD_MORNING":
                print("MORNING SOUND")
                assistant.speak_out("Selamat Pagi, " + message.get('content'))

            if message.get('request_type') == "MEDIA":
                file = message.get('content').get("file")
                status_media = message.get('content').get("status")
                media_player.change_player_state(file, status_media)

    print("net service")
    # network service
    net_service = NetworkService()
    def net_serve():
        print("init net")
        net_service.receive_thread(
            event_separator
        )
    net_thread = threading.Thread(target=net_serve)
    net_thread.start()




