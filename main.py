import threading
from EventManager import event_manager;
# import RPi.GPIO as GPIO
from Manager.Security import Security

try:
    # GPIO.setmode(GPIO.BOARD)
    # GPIO.setwarnings(False)
    print("System Starting")

    main_thread = threading.Thread(name="System Start", target=event_manager)
    main_thread.start()

    # initialize Security
    security = Security(22)
    security_thread = threading.Thread(name="Security Thread", target=security.bell_listener)
    security_thread.start

    print("thread started")
    tester = input("tester")
    if tester == "1":
        print("notify_master");
        security.notify_master()
except KeyboardInterrupt:
    print("EXITING")
    # GPIO.cleanup()