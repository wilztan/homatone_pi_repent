from System import config
from socketIO_client_nexus import SocketIO


class NetworkService:
    def __init__(self):
        self.url = config.url
        self.socket_io = SocketIO(self.url)
        print("Initializing Socket "+self.url)

    def receive_thread(self, service):
        self.socket_io.on(config.proxy, service)
        self.socket_io.wait()

    def post_service(self):
        pass

    def get_service(self):
        pass
