from Manager.TemperatureInfraRed import Temperature
from socketIO_client_nexus import SocketIO, LoggingNamespace
from System import config
import threading


def temperature_service():
    url = config.url;
    socket_io = SocketIO(url);

    # Air Conditioner
    air_conditioner_main = Temperature("Main AC", 32)
    air_conditioner_thread = threading.Thread(
        name=air_conditioner_main.name,
        target=air_conditioner_main.start_machine
    )
    air_conditioner_thread.start();

    def temperature_manager(message):
        print(message);
        if message.get('hardware_id') == config.hardware_id and message.get('request_type') == "IRMESSAGE":
            if message.get('content') == "STATUSAC:false":
                air_conditioner_main.turn_on()
            if message.get('content') == "STATUSAC:true":
                air_conditioner_main.turn_off()

    # SOCKET RECEIVER
    socket_io.on(config.proxy, temperature_manager);
    socket_io.wait();