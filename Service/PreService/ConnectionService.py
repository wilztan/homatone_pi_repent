from System import config
from socketIO_client_nexus import SocketIO
from Manager.Downloader import Downloader


def connection_service():
    url = config.url
    socket_io = SocketIO(url)

    downloader = Downloader()

    def connection_manager(message):
        if message.get('hardware_id') == config.hardware_id:
            if message.get('request_type') == "DOWNLOAD":
                file = message.get('content').get("file")
                name = message.get('content').get("name")
                id = message.get('content').get("id")
                downloader.download()

    # SOCKET RECEIVER
    socket_io.on(config.proxy, connection_manager)
    socket_io.wait()