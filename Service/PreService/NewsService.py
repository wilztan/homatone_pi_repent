from Manager.Assistant import Assistant
from socketIO_client_nexus import SocketIO, LoggingNamespace
from System import config


def news_service():
    url = config.url
    socket_io = SocketIO(url)

    assistant = Assistant();

    # LISTENING TO NEWS
    def news_manager(message):
        print(message);
        if message.get('hardware_id') == config.hardware_id and message.get('request_type') == "NEWS_READ":
            print("VOICE OUT NEWS")
            assistant.read_news()
        if message.get('hardware_id') == config.hardware_id and message.get('request_type') == "GOOD_MORNING":
            print("MORNING SOUND")
            assistant.speak_out("Selamat Pagi, "+message.get('content'));

    # SOCKET RECEIVER
    socket_io.on(config.proxy, news_manager);
    socket_io.wait();
