from System import config
from socketIO_client_nexus import SocketIO
from Manager.Media import Media
import threading


def media_service():
    url = config.url
    socket_io = SocketIO(url)

    media_player = Media()
    media_player_thread = threading.Thread(name="Media Player", target=media_player.start_media_player)
    media_player_thread.start();

    def media_manager(message):
        if message.get('hardware_id') == config.hardware_id:
            if message.get('request_type') == "MEDIA":
                file = message.get('content').get("file")
                status_media = message.get('content').get("status")
                media_player.change_player_state(file, status_media)

    # SOCKET RECEIVER
    socket_io.on(config.proxy, media_manager)
    socket_io.wait()