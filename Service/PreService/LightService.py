from socketIO_client_nexus import SocketIO, LoggingNamespace
from System import config
from Manager.Light import Light
import threading


def light_service():

    url = config.url
    socket_io = SocketIO(url);

    main_room_light = Light("Main Room", 7)
    toilet_room_light = Light("Toilet Room", 15)
    bed_room_light = Light("Bed Room", 11)

    main_room_thread = threading.Thread(name="Main Room", target=main_room_light.start_light)
    bed_room_thread = threading.Thread(name="Bed Room", target=bed_room_light.start_light)
    toilet_room_thread = threading.Thread(name="Toilet Room", target=toilet_room_light.start_light)

    main_room_thread.start()
    bed_room_thread.start()
    toilet_room_thread.start()

    def light_manager(message):
        if message.get('hardware_id') == config.hardware_id and message.get('request_type') == "LIGHTS_MANAGE":
            if message.get('content') == "LIGHTS_MAIN_ON":
                main_room_light.lights_on()
            if message.get('content') == "LIGHTS_MAIN_OFF":
                main_room_light.lights_off()
            if message.get('content') == "LIGHTS_BED_ON":
                bed_room_light.lights_on()
            if message.get('content') == "LIGHTS_BED_OFF":
                bed_room_light.lights_off()
            if message.get('content') == "LIGHTS_TOILET_ON":
                toilet_room_light.lights_on()
            if message.get('content') == "LIGHTS_TOILET_OFF":
                toilet_room_light.lights_off()

    # SOCKET RECEIVER
    socket_io.on(config.proxy, light_manager)
    socket_io.wait()
