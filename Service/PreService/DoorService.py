from socketIO_client_nexus import SocketIO, LoggingNamespace
from System import config
from Manager.Door import Door


def door_service():
    url = config.url
    socket_io = SocketIO(url);

    main_door = Door("Main Door", 12, 2.5, 5)
    window_door = Door("Window Door", 16, 5, 17.5)
    bed_room_door = Door("Bedroom Door", 13, 10, 2.5)

    def door_manager(message):
        print("Received "+str(message));
        if message.get('hardware_id') == config.hardware_id and message.get('request_type') == "DOOR_OPEN":

            if message.get('content') == "DOOR_MAIN":
                main_door.open_close()

            if message.get('content') == "WINDOWS_MAIN_OPEN":
                window_door.open()

            if message.get('content') == "WINDOWS_MAIN_CLOSE":
                window_door.close()

            if message.get('content') == "DOOR_BED_OPEN":
                bed_room_door.open()

            if message.get('content') == "DOOR_BED_CLOSE" :
                bed_room_door.close()

    # SOCKET RECEIVER
    socket_io.on(config.proxy, door_manager)
    socket_io.wait()

