import threading
import RPi.GPIO as GPIO

from Service.PreService import NewsService, TemperatureService, LightService, SecurityService, MediaService, \
    DoorService, ConnectionService

try:
    GPIO.setmode(GPIO.BOARD)
    GPIO.setwarnings(False)
    print("System Starting")

    door_thread_management = threading.Thread(name="Door Management", target=DoorService.door_service)
    light_thread_management = threading.Thread(name="Light Management", target=LightService.light_service)
    news_thread_management = threading.Thread(name="News Management", target=NewsService.news_service)
    media_thread_management = threading.Thread(name="Media Management", target=MediaService.media_service)
    security_thread_management = threading.Thread(name="Security Management", target=SecurityService.security_service)
    connection_thread_management = threading.Thread(name="Connection Management", target=ConnectionService.connection_service)
    temperature_thread_management = threading.Thread(
        name="Temperature Management", target=TemperatureService.temperature_service)

    door_thread_management.start()
    light_thread_management.start()
    temperature_thread_management.start()
    news_thread_management.start()
    media_thread_management.start()
    connection_thread_management.start()
    security_thread_management.start()

    print("thread started")
    input()
except KeyboardInterrupt:
    print("EXITING");
    # GPIO.cleanup()
