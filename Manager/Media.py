import pygame

from Manager.Manager import Manager


class Media(Manager):
    def __init__(self):
        self.media = ""
        self.status = "STOP"
        pygame.init()
        pygame.mixer.init()
        print("Initialize Media")

    def start_media_player(self):
        while True:
            if self.media != "":
                pygame.mixer.music.load(self.media)
                pygame.mixer.music.play()
                while pygame.mixer.music.get_busy():
                    if self.status == "PLAY":
                        pygame.time.Clock().tick(10)
                    if self.status == "PAUSE":
                        break;
                    if self.status == "STOP":
                        break;

    def change_media(self, media):
        self.media = media

    def change_player_state(self, media, status):
        self.media = media
        self.status = status
        print("Change Player State");

    def play(self):
        self.status = "PLAY"

    def stop(self):
        self.status == "STOP"
