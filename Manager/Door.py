import time
# import RPi.GPIO as GPIO
from Manager.Manager import Manager


class Door(Manager):

    def __init__(self, name, port, open_radius, close_radius):
        self.name = name
        self.port = port
        self.open_radius = open_radius
        self.close_radius = close_radius
        # GPIO.setup(port, GPIO.OUT)
        # self.p =GPIO.PWM(port,50)

    # Start at 5
    def start(self):
        print("Start "+self.name)
        # self.p.start(self.close_radius)

    def open(self):
        print("Open "+self.name)
        # self.p.ChangeDutyCycle(self.open_radius)

    def close(self):
        print("Close "+self.name)
        # self.p.ChangeDutyCycle(self.close_radius)

    def open_close(self):
        print("Open Close "+self.name)
        # self.p.ChangeDutyCycle(self.open_radius)
        # time.sleep(2)
        # self.p.ChangeDutyCycle(self.close_radius)
