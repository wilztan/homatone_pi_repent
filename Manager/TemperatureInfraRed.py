from time import sleep
# import RPi.GPIO as GPIO
from Manager.Manager import Manager


class Temperature(Manager):
    def __init__(self, name, port):
        self.name = name
        self.port = port
        self.status = 1
        self.temperature = 0
        print("Initializing "+self.name)
        # GPIO.setup(port, GPIO.OUT)

    def start_machine(self):
        print("Start Machine "+self.name)
        while True:
            # print(self.name + " Status " + str(self.status))
            pass
            # GPIO.output(self.port, self.status)
            # sleep(0.5);
            # GPIO.setup(self.port, GPIO.OUT)

    def change_heat(self, temperature):
        self.temperature = temperature
        print("Temperature Changed "+str(temperature))

    def turn_on(self):
        self.status = 0
        print("Temperature Changed On")

    def turn_off(self):
        self.status = 1
        print("Temperature Changed Off")
