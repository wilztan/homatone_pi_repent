from time import sleep
# import RPi.GPIO as GPIO
from Manager.Manager import Manager


class Light(Manager):

    def __init__(self, name, port):
        self.name = name
        self.port = port
        self.status = 1
        print("Initializing "+self.name)
        # GPIO.setup(port, GPIO.OUT)

    def get_name(self):
        return self.name

    def get_port(self):
        return self.port

    def start_light(self):
        print("Start Light "+self.name)
        while True:
            # print(self.name + " Status " + str(self.status))
            pass
            # GPIO.output(self.port, self.status)
            # sleep(0.5);
            # GPIO.setup(self.port, GPIO.OUT)

    def lights_on(self):
        self.status = 0
        print(self.name + " Light Changed " + str(self.status))

    def lights_off(self):
        self.status = 1
        print(self.name + " Light Changed " + str(self.status))
