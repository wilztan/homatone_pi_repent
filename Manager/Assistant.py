from subprocess import call
import requests
import json

from Manager.Manager import Manager
from System import config

cmd_beg = 'espeak -vid+f1 -s150 "'
# cmd_beg= 'espeak -ven-us -s180 "'
cmd_end = '" 2>/dev/null'


class Assistant(Manager):
    def __init__(self):
        print("Initialize Assistant")

    def fetch_api(self, url):
        r = requests.get(url)
        data = json.loads(r.content.decode())
        return  data

    def read_news(self):
        url = config.url+config.news_url;
        data = self.fetch_api(url)
        news = data["articles"];
        self.speak_out("Hello tuan");
        self.speak_out("Berita hari ini");
        for i in range(0, 10):
            title = news[i]["title"];
            title.replace("-", ", oleh ");
            self.speak_out(title);
        self.voiceOut("Berita selesai");


    def speak_out(self, words):
        call([cmd_beg+words+cmd_end], shell=True)

    def speak_out_with_speed_control(self, words, speed):
        call(['espeak -vid+f1 -s' + speed + ' "' + words + cmd_end], shell=True)