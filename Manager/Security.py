import requests
import time
import datetime
import cv2
import os

from Manager.Manager import Manager
from System import config
from Manager.Assistant import Assistant
faceDetect = cv2.CascadeClassifier('haarcascade_frontalface_default.xml');


class Security(Manager):

    def __init__(self, port):
        # port 22
        self.port = port
        self.assistant = Assistant()
        print("Initialize Security")

    def bell_listener(self):
        print("bell listener")
        while True:
            input_state = True #GPIO.input(self.port)
            if input_state == False:
                print('Button Pressed')
                # self.assistant.speak_out("Ada yang datang")
                # self.notify_master()
        # GPIO.setup(self.port, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def notify_master(self):
        image, name = self.image_capture()
        message = "captured ",
        if name != "":
            message = "captured "+name+" is comming"
        print("file name : " + image)
        try:
            files = {'media': open(image, 'rb')}
        except:
            print("failed in opening")
            files = ''
        if image == '':
            message = "something wrong with images"
        headers = {'content': str(message), 'hardware_id': str(config.hardware_id), 'media': str(image),
                   'user': "guest"}
        payload = {'message': message}
        result = requests.post(config.notification_url, files=files, headers=headers, data=payload)
        print(result)
        time.sleep(0.2)

    def image_capture(self,):
        cam = cv2.VideoCapture(0)
        # width=640
        cam.set(3, 640)
        # height=480
        cam.set(4, 480)
        print("start")

        if cam.isOpened():
            # _,frame = cam.read()
            # detect
            print("read")
            ret, img = cam.read()
            gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            faces = faceDetect.detectMultiScale(gray, 1.3, 5)
            name = ""
            for (x, y, w, h) in faces:
                cv2.rectangle(img, (x, y), (x + w, y + h), (0, 0, 255), 2)
                name = self.detect_user(faces)
            cam.release()  # releasing camera immediately after capturing picture
            if ret and img is not None:
                print("writing")
                file_to_send = self.generate_file_name()
                filename = os.path.join(config.dir_name, file_to_send)
                cv2.imwrite(filename, img)
                return filename, name
            return '', ''

    def generate_file_name(self ):
        currentDT = datetime.datetime.now()
        return "image" + str(currentDT) + ".jpg";

    def detect_user(self, faces):
        return ''

    def train_detector(self):
        pass